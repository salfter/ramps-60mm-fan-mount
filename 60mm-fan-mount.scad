$fn=30;

// test object: 60mm fan
translate([35-.75,60,31])
    % import("Fan_60x60x15.stl");

difference()
{
    union()
    {
        // original to modify
        import("ramps_fan_mount_v1_0.stl");

        // plug original holes

        translate([16,50,5])
        rotate([90,0,0])
            cylinder(d=3.4, h=3.5);

        translate([48,50,5])
        rotate([90,0,0])
            cylinder(d=3.4, h=3.5);

        // make flat spots for mounting

        translate([-.75,0,0])
        {
            translate([7.75,45.5,5])
                cube([8,9,10], center=true);

            translate([57.75,45.5,5])
                cube([8,9,10], center=true);
        }
    }

    translate([-.75,0,0])
    {
        // enlarge cutout for fan

        translate([33,50.5,31])
        rotate([90,0,0])
            cylinder(d=56, h=4.5, $fn=180);

        // drill new mounting holes

        translate([7.75,80,6])
        rotate([90,0,0])
            cylinder(d=3.4, h=50);

        translate([57.75,80,6])
        rotate([90,0,0])
            cylinder(d=3.4, h=50);
    }
}

